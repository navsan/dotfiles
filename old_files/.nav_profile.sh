#!/bin/bash

alias vi="vim"
export EDITOR=vim
export JAVA_HOME=/usr
#export GCC_HOME=/usr/local/Cellar/gcc/5.1.0
export PGDATABASE=tpch
export PREFIX=~/Installs
export PATH=$PREFIX:$PATH

# # Re-binding ^W so that it works well with file paths 
# stty werase undef
# bind '"\C-w":backward-kill-word'

####################### Clang+LLVM Stuff #######################
export LLVM_HOME=/opt/clang-llvm
export PATH=$LLVM_HOME/bin:$PATH
export CPATH=/usr/local/include/c++/v1:$LLVM_HOME/include:$CPATH
export LIBRARY_PATH=$LLVM_HOME/lib:/usr/local/lib:/usr/lib:$LIBRARY_PATH
export LD_LIBRARY_PATH=$LLVM_HOME/lib:/usr/local/lib:/usr/lib:$LD_LIBRARY_PATH
#export DYLD_LIBRARY_PATH=$LLVM_HOME/lib:/usr/local/lib:/usr/lib:$DYLD_LIBRARY_PATH 

####################### GPDB Stuff #######################
# # For compiling GPDB
# #export PATH=/opt/gcc-4.4.2-osx109_x86_32/bin:/opt/gcc_infrastructure-osx109_x86_32/bin:$PATH
# #export DYLD_LIBRARY_PATH=/opt/gcc_infrastructure-osx109_x86_32/lib:$DYLD_LIBRARY_PATH:/usr/local/lib
# export BLD_ARCH=osx106_x86
# 
# # For running GPDB
# export GPHOME=~/Programming/gpdb/gpdb-prof/greenplum-db-devel
# export GPDATA=~/gpdb-data
# export PATH=$PATH:$GPHOME/bin
# ulimit -n 65536
# 
# if [ -e $GPHOME/greenplum_path.sh ]; then
# 	: 
# 	#source $GPHOME/greenplum_path.sh
# fi
# export MASTER_DATA_DIRECTORY=$GPDATA/master/gpseg-1
# 
# # GPDB comes with its own (older) libz, but that doesn't with the 
# # (newer) system library libPng. Hence this hack.
# #export DYLD_LIBRARY_PATH=/opt/hacky-libs/:$DYLD_LIBRARY_PATH
# 
# # Default GPDB database to connect to (for convenience, to avoid using -d)
# export PGDATABASE=tpch
# 
# # greenplum_path.sh destroys the PYTHONPATH. I'm using the default PYTHONPATH (implicit, undeclared)
# # using python -c "import sys; print(':'.join(sys.path))"
# export PYTHONPATH=$PYTHONPATH:/usr/local/Cellar/python/2.7.10/Frameworks/Python.framework/Versions/2.7/lib/python27.zip:/usr/local/Cellar/python/2.7.10/Frameworks/Python.framework/Versions/2.7/lib/python2.7:/usr/local/Cellar/python/2.7.10/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-darwin:/usr/local/Cellar/python/2.7.10/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac:/usr/local/Cellar/python/2.7.10/Frameworks/Python.framework/Versions/2.7/lib/python2.7/plat-mac/lib-scriptpackages:/usr/local/Cellar/python/2.7.10/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-tk:/usr/local/Cellar/python/2.7.10/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-old:/usr/local/Cellar/python/2.7.10/Frameworks/Python.framework/Versions/2.7/lib/python2.7/lib-dynload:/usr/local/lib/python2.7/site-packages:/usr/local/Cellar/protobuf/2.6.1/libexec/lib/python2.7/site-packages:/Library/Python/2.7/site-packages/pip-7.1.0-py2.7.egg:/Library/Python/2.7/site-packages
# 

# ####################### Perforce Stuff #######################
# export P4PORT=perforce.greenplum.com:1666
# export P4USER=npotti

####################### iTerm2 Stuff #######################
# iTerm seems to mangle my $SHELL env var. Fixing it here
export SHELL=`dscl . -read /Users/npotti UserShell  | cut -d " " -f 2-`

renametab () {
	echo -e "\033];$@\007"
}

