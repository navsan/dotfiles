# Environment variables for ZSH
# Having this in .zshenv rather than .zshrc ensures that these env vars are set
# even in non-interactive shells (like Emacs app in OSX)
# https://github.com/syl20bnr/spacemacs/issues/3920

#----------------------------------------------------------------------------
#                   PATH
#----------------------------------------------------------------------------
# rust binaries through cargo (currently for racer)
export PATH=$PATH:~/.cargo/bin
export RUST_SRC_PATH=~/src/rust-lang/src
