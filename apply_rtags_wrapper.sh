#/bin/bash

set -e 

WRAPPER_DIR=/home/vagrant/src/rtags/bin/rtags-wrapped-compilers 
RTAGS_BIN_DIR=/usr/local/bin

# remove existing links
mkdir -p $WRAPPER_DIR
rm -f $WRAPPER_DIR/clang 
rm -f $WRAPPER_DIR/clang++

base_clang=`which clang`
wrapped_clang=$WRAPPER_DIR/clang 

ln -s $RTAGS_BIN_DIR/gcc-rtags-wrapper.sh ${wrapped_clang}
ln -s $RTAGS_BIN_DIR/gcc-rtags-wrapper.sh ${wrapped_clang}++


echo "PATH is $PATH\n"
echo "$RTAGS_BIN_DIR/gcc-rtags-wrapper.sh now wraps "
echo "$base_clang to "
echo "$wrapped_clang"


