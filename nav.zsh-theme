turquoise="%F{81}"
orange="%F{166}"
purple="%F{135}"
hotpink="%F{161}"
limegreen="%F{118}"

local ret_status="%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}➜ )"

ZSH_THEME_GIT_PROMPT_PREFIX="%{$purple%}["
ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%} "
ZSH_THEME_GIT_PROMPT_DIRTY=" %{$fg_bold[red]%}✗%{$purple%}]"
ZSH_THEME_GIT_PROMPT_CLEAN="%{$purple%}]"
ZSH_THEME_GIT_PROMPT_UNKNOWN=" %{$fg_bold[red]%}??%{$purple%}"

git_dirty_unknown() {
    if [[ "$(command git config --get oh-my-zsh.hide-dirty)" -eq "1" ]]; then
        echo "$ZSH_THEME_GIT_PROMPT_UNKNOWN"
    else
        echo ""
    fi
}


PROMPT=$'
${ret_status}%{$reset_color%} %{$fg[yellow]%}%D{[%K:%M:%S]} %{$purple%}%n%{$reset_color%} at %{$orange%}%m%{$reset_color%} in %{$turquoise%}%~%{$reset_color%} $(git_prompt_info)$(git_dirty_unknown)%{$reset_color%}
$ '
