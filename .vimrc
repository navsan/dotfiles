set nocompatible
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

"Plugin 'tpope/vim-fugitive'

Plugin 'majutsushi/tagbar'

Plugin 'scrooloose/syntastic'

Plugin 'terryma/vim-smooth-scroll'

"Plugin 'Valloric/YouCompleteMe'

Plugin 'lyuts/vim-rtags'

Plugin 'marijnh/tern_for_vim'

Plugin 'brookhong/cscope.vim'

Plugin 'rdnetto/YCM-Generator'

Plugin 'nathanaelkane/vim-indent-guides'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

" Configuration file for vim
set modelines=0		" CVE-2007-2438

" Set the <Leader> key
let mapleader = ","

" Normally we use vim-extensions. If you want true vi-compatibility
" remove change the following statements
set nocompatible	" Use Vim defaults instead of 100% vi compatibility
set backspace=2		" more powerful backspacing

" Don't write backup file if vim is being called by "crontab -e"
au BufWrite /private/tmp/crontab.* set nowritebackup nobackup
" Don't write backup file if vim is being called by "chpass"
au BufWrite /private/etc/pw.* set nowritebackup nobackup
" Remove trailing whitespace on save
au BufWritePre * %s/\s\+$//e

" Added by NAV on 3Jul15
syntax on
set nu
set expandtab
set tabstop=2
set shiftwidth=2
set hlsearch
noh
set ignorecase
set showmatch
set autoindent
set smartindent
set mouse=a
set cursorline
set colorcolumn=80

" for tagbar
nmap <F8> :TagbarToggle<CR>
noremap <silent> <F9> :TagbarOpen fj<CR>
let g:tagbar_autoclose = 1
"let g:tagbar_show_linenumbers = 1
"autocmd FileType c,cpp,h,hpp,py nested :TagbarOpen



" Because I keep making this typo
:command! Wq wq
:command! W  w

" Faster scrolling within the line
:noremap  <silent> <C-a>      <Home>
" :noremap  <silent> <C-e>      <End>
:noremap <silent> <C-s>      <End>
:inoremap <silent> <C-a>      <Home>
:inoremap <silent> <C-e>      <End>
:inoremap <silent> <C-s>      <End>
:inoremap <silent> <C-f>      <C-O>f
:inoremap <silent> <C-b>      <C-O>F
:inoremap <silent> <C-n>      <C-O>;
:inoremap <silent> <M-w>      <C-O>w
:inoremap <silent> <M-b>      <C-O>b

" Indent Guides
let g:indent_guides_auto_colors = 0
let g:indent_guides_guide_size = 0
let g:indent_guides_enable_on_vim_startup = 1
let g:indent_guides_start_level = 2
let g:indent_guides_exclude_filetypes = ['help']
autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  ctermbg=234
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven ctermbg=236

" let g:zenburn_high_Contrast=1
" colorscheme zenburn
colorscheme wasabi256

" To enable powerline
set rtp+=/usr/local/lib/python2.7/dist-packages/powerline/bindings/vim/
set laststatus=2
set t_Co=256

" Macvim screws up colors if you don't do this :(
let macvim_skip_colorscheme=1


" Global YCM config file, so it stops bugging me
let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/.ycm_extra_conf.py'

" Close the annoying preview window
let g:ycm_autoclose_preview_window_after_completion = 1

" Stop askking for confirmation
let g:ycm_confirm_extra_conf = 0

" Do't store global/local values and folds mn a session
set ssop-=options
set ssop-=folds


" Paste toggle to help copy-paste without auto-indent
set pastetoggle=<F2>

" File Type specific settings
au FileType cpp set sw=2 ts=2 sts=2
au FileType python set sw=2 ts=2 sts=2

" Ignore certain files from auto-completion
set wildignore+=*.bc,*.bc_,*.o,*.a,*.dylib

" More visual feedback while typing commands
set showcmd

if has("cscope")
	set cscopetagorder=1
	set cst
	set cscopetag
	set nocsverb
	"set csto=1
    " add any cscope database in current directory
    if filereadable("cscope.out")
        cs add cscope.out
	else
   		let cscope_file=findfile("cscope.out", ".;")
   		let cscope_pre=matchstr(cscope_file, ".*/")
   		if !empty(cscope_file) && filereadable(cscope_file)
      		exe "cs add" cscope_file cscope_pre
   		endif
	endif
endif
