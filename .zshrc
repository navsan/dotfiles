export ZSH=$HOME/.oh-my-zsh

# Theme to load from ~/.oh-my-zsh/themes/
ZSH_THEME="nav"

# Disable marking untracked files under VCS as dirty. 
# Speeds up repository status check for large repositories.
DISABLE_UNTRACKED_FILES_DIRTY="true"

plugins=(git vi-mode colored-man-pages)

#----------------------------------------------------------------------------
#                  local-config 
#----------------------------------------------------------------------------

if [[ -f $HOME/local.zshrc ]]; then
    source $HOME/local.zshrc
fi


#----------------------------------------------------------------------------
#                  Oh-my-zsh
# At the end to allow local-config to override settings. 
#----------------------------------------------------------------------------

source $ZSH/oh-my-zsh.sh

#----------------------------------------------------------------------------
#                   tmux
#----------------------------------------------------------------------------

function tmux_create_standard_layout() {
    tmux split-window -h -p 35
    tmux split-window -v
    tmux select-pane -L
}

# Start tmux session "default" if it doesn't exist, attach to it.
# Pass in a session name to use something other than "default" 
function _tmux_do_the_right_thing() {
    local session_name="default"
    if [[ -n "$1" ]]; then
        session_name=$1
	  fi

    if [[ -n ${TMUX+x} ]]; then
        tmux_create_standard_layout
    elif
        [[ $(tmux list-sessions 2>&1 | grep "^${session_name}:" | wc -l) -eq "0" ]]; then
        tmux new-session -d -s ${session_name}
        tmux_create_standard_layout
    fi
    tmux -2 attach -t ${session_name}
}
alias t=_tmux_do_the_right_thing

#----------------------------------------------------------------------------
#                   zsh/bash additional settings
#
#----------------------------------------------------------------------------
stty -ixon

#----------------------------------------------------------------------------
#                   zsh vi-mode key bindings
# http://zsh.sourceforge.net/Doc/Release/Zsh-Line-Editor.html#Modifying-Text
#----------------------------------------------------------------------------

bindkey "^a" vi-beginning-of-line
bindkey "^s" vi-end-of-line 
bindkey "^e" vi-end-of-line 
bindkey '^R' history-incremental-pattern-search-backward
bindkey '^[[3~' delete-char
bindkey '^[d' delete-word


#----------------------------------------------------------------------------
#                   aliases
#----------------------------------------------------------------------------

alias gc="git commit -m"
alias gs="git status -bs"
alias e="emacsclient -a '' -t"
alias ekill="emacsclient -e '(kill-emacs)'"

#----------------------------------------------------------------------------
#                   PATH
#  (set in .zshenv instead of here)
#----------------------------------------------------------------------------
