;;; packages.el --- cpp-rtags layer packages file for Spacemacs.
;;
;; URL: https://github.com/syl20bnr/spacemacs
;;
;; This file is not part of GNU Emacs.
;;
;;; License: GPLv3

;;; Commentary:

;; See the Spacemacs documentation and FAQs for instructions on how to implement
;; a new layer:
;;
;;   SPC h SPC layers RET
;;
;;
;; Briefly, each package to be installed or configured by this layer should be
;; added to `cpp-rtags-packages'. Then, for each package PACKAGE:
;;
;; - If PACKAGE is not referenced by any other Spacemacs layer, define a
;;   function `cpp-rtags/init-PACKAGE' to load and initialize the package.

;; - Otherwise, PACKAGE is already referenced by another Spacemacs layer, so
;;   define the functions `cpp-rtags/pre-init-PACKAGE' and/or
;;   `cpp-rtags/post-init-PACKAGE' to customize the package as it is loaded.

;;; Code:

(defconst cpp-rtags-packages
  '(rtags)
  "The list of Lisp packages required by the rtags layer.

Each entry is either:

1. A symbol, which is interpreted as a package to be installed, or

2. A list of the form (PACKAGE KEYS...), where PACKAGE is the
    name of the package to be installed or loaded, and KEYS are
    any number of keyword-value-pairs.

    The following keys are accepted:

    - :excluded (t or nil): Prevent the package from being loaded
      if value is non-nil

    - :location: Specify a custom installation location.
      The following values are legal:

      - The symbol `elpa' (default) means PACKAGE will be
        installed using the Emacs package manager.

      - The symbol `local' directs Spacemacs to load the file at
        `./local/PACKAGE/PACKAGE.el'

      - A list beginning with the symbol `recipe' is a melpa
        recipe.  See: https://github.com/milkypostman/melpa#recipe-format")

(defun rtags-evil-standard-keybindings (mode)
  ;; (evil-leader/set-key-for-mode mode
  ;; (debug)
  (spacemacs/set-leader-keys-for-major-mode mode
  	"R." 'rtags-find-symbol-at-point
    "R," 'rtags-find-references-at-point
    "Rv" 'rtags-find-virtuals-at-point
    "RV" 'rtags-print-enum-value-at-point
    "R/" 'rtags-find-all-references-at-point
    "RY" 'rtags-cycle-overlays-on-screen
    "R>" 'rtags-find-symbol
    "R<" 'rtags-find-references
    "R[" 'rtags-location-stack-back
    "R]" 'rtags-location-stack-forward
    "RD" 'rtags-diagnostics
    "RG" 'rtags-guess-function-at-point
    "Rp" 'rtags-set-current-project
    "RP" 'rtags-print-dependencies
    "Re" 'rtags-reparse-file
    "RE" 'rtags-preprocess-file
    "RR" 'rtags-rename-symbol
    "RM" 'rtags-symbol-info
    "RS" 'rtags-display-summary
    "RO" 'rtags-goto-offset
    "R;" 'rtags-find-file
    "RF" 'rtags-fixit
    "RL" 'rtags-copy-and-print-current-location
    "RX" 'rtags-fix-fixit-at-point
    "RB" 'rtags-show-rtags-buffer
    "RI" 'rtags-imenu
    "RT" 'rtags-taglist
    "Rh" 'rtags-print-class-hierarchy
    "Ra" 'rtags-print-source-arguments
    ))

;; For each package, define a function cpp-rtags/init-<package-name>
;;
(defun cpp-rtags/init-rtags ()
  (use-package rtags)
  (require 'flycheck-rtags)
  (require 'company-rtags)
  (push 'company-rtags company-backends-c-mode-common)
  (setq rtags-completions-enabled t)
  (rtags-evil-standard-keybindings 'c-mode)
  (rtags-evil-standard-keybindings 'c++-mode)

  (add-hook 'c-mode-common-hook 'rtags-start-process-unless-running)
  (add-hook 'c++-mode-common-hook 'rtags-start-process-unless-running)
  )

(defun c-c++/post-init-rtags ()
  (when c-c++-enable-rtags-support
    (setq company-rtags-begin-after-member-access nil)

     (defun use-rtags (&optional useFileManager)
       (and (rtags-executable-find "rc")
            (cond ((not (gtags-get-rootpath)) t)
                  ((and (not (eq major-mode 'c++-mode))
                        (not (eq major-mode 'c-mode))) (rtags-has-filemanager))
                  (useFileManager (rtags-has-filemanager))
                  (t (rtags-is-indexed)))))

     (defun tags-find-symbol-at-point (&optional prefix)
       (interactive "P")
       (if (and (not (rtags-find-symbol-at-point prefix)) rtags-last-request-not-indexed)
           (helm-gtags-find-tag)))

     (defun tags-find-references-at-point (&optional prefix)
       (interactive "P")
       (if (and (not (rtags-find-references-at-point prefix)) rtags-last-request-not-indexed)
           (helm-gtags-find-rtag)))

     (defun tags-find-symbol ()
       (interactive)
       (call-interactively (if (use-rtags) 'rtags-find-symbol 'helm-gtags-find-symbol)))

     (defun tags-find-references ()
       (interactive)
       (call-interactively (if (use-rtags) 'rtags-find-references 'helm-gtags-find-rtag)))

     (defun tags-find-file ()
       (interactive)
       (call-interactively (if (use-rtags t) 'rtags-find-file 'helm-gtags-find-files)))

     (defun tags-imenu ()
       (interactive)
       (call-interactively (if (use-rtags t) 'rtags-imenu 'idomenu)))
	
     (rtags-enable-standard-keybindings)
     (define-key c-mode-base-map (kbd "M-.") (function tags-find-symbol-at-point))
     (define-key c-mode-base-map (kbd "M-,") (function tags-find-references-at-point))
     (define-key c-mode-base-map (kbd "M-;") (function tags-find-file))
     (define-key c-mode-base-map (kbd "C-.") (function tags-find-symbol))
     (define-key c-mode-base-map (kbd "C-,") (function tags-find-references))
     (define-key c-mode-base-map (kbd "C-<") (function rtags-find-virtuals-at-point))
     (define-key c-mode-base-map (kbd "M-i") (function tags-imenu))

     (define-key global-map (kbd "M-.") (function tags-find-symbol-at-point))
     (define-key global-map (kbd "M-,") (function tags-find-references-at-point))
     (define-key global-map (kbd "M-;") (function tags-find-file))
     (define-key global-map (kbd "C-.") (function tags-find-symbol))
     (define-key global-map (kbd "C-,") (function tags-find-references))
     (define-key global-map (kbd "C-<") (function rtags-find-virtuals-at-point))
     (define-key global-map (kbd "M-i") (function tags-imenu))))



;;; packages.el ends here
